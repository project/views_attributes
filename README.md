# Views Attributes

Manage HTML attributes on a views display. Works alongside the included "CSS class" option. Useful for adding
arbitrary attributes, such as datasets, to a rendered views display.

## Usage

1. Download and install the `drupal/views_attributes` module. Recommended install method is composer:
   ```
   composer require drupal/views_attributes
   ```
2. Go to `/admin/structure/views/settings/advanced` to ensure the "Attributes" display extender is enabled.
3. Click the "Attributes" link in the "Other" section of the views configuration form.
4. Add, remove and re-order attributes as needed and then click "Apply".
