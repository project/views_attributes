<?php

namespace Drupal\views_attributes\Plugin\views\display_extender;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;

/**
 * Implementation of 'attributes' display extender plugin.
 *
 * @ingroup views_display_extender_plugins
 *
 * @ViewsDisplayExtender(
 *   id = "attributes",
 *   title = @Translation("Attributes"),
 *   help = @Translation("Manage HTML attributes on a views display."),
 *   no_ui = FALSE
 * )
 */
class AttributesDisplayExtender extends DisplayExtenderPluginBase {

  /**
   * Protected attribute names.
   */
  const PROTECTED_ATTRIBUTES = ['add', 'class'];

  /**
   * {@inheritdoc}
   */
  public function defineOptionsAlter(&$options) {
    $options['attributes'] = ['default' => []];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    if ($form_state->get('section') === 'attributes') {
      $form['#title'] = 'Attributes';
      $form['attributes'] = [
        '#type' => 'table',
        '#header' => [
          $this->t('Attribute'),
          $this->t('Value'),
          NULL,
          $this->t('Weight'),
        ],
        '#empty' => $this->t('No attributes.'),
        '#tabledrag' => [
          [
            'action' => 'order',
            'relationship' => 'sibling',
            'group' => 'table-sort-weight',
          ],
        ],
        '#prefix' => '<div id="views-display-attributes">',
        '#suffix' => '</div>',
        '#tree' => TRUE,
      ];
      if (!$values = $form_state->getValue('attributes')) {
        $input = $form_state->getUserInput();
        // Preserve attributes from user input or default values.
        $values = $input['attributes'] ?? $this->options['attributes'];
      }
      if ($values) {
        uasort($values, [SortArray::class, 'sortByWeightElement']);
      }
      foreach ($values as $name => $options) {
        $form['attributes'][$name] = $this->buildElements($name, $options);
      }
      $form['attributes']['add'] = $this->buildElements();
    }
  }

  /**
   * Build attribute elements.
   *
   * @param string|null $name
   *   The attribute name.
   * @param array $options
   *   The attribute options.
   *
   * @return array
   *   Returns an array of form elements.
   */
  protected function buildElements(string $name = NULL, array $options = []): array {
    $elements = [
      '#attributes' => ['class' => ['draggable']],
      '#weight' => $options['weight'] ?? 100,
      '#element_validate' => [[static::class, 'validateAttribute']],
    ];
    $elements['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#title_display' => 'invisible',
      '#default_value' => $name,
      '#disabled' => $name !== NULL,
    ];
    $elements['value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value'),
      '#title_display' => 'invisible',
      '#default_value' => $options['value'] ?? NULL,
    ];
    $elements['op'] = [
      '#type' => 'submit',
      '#ajax' => [
        'wrapper' => 'views-display-attributes',
        'callback' => [self::class, 'ajax'],
        'effect' => 'fade',
      ],
      '#button_type' => 'small',
    ];
    if (!$name) {
      $elements['op']['#value'] = $this->t('Add');
      $elements['op']['#submit'] = [[self::class, 'addSubmit']];
    }
    else {
      $elements['op']['#value'] = $this->t('Remove');
      $elements['op']['#name'] = "remove_$name";
      $elements['op']['#limit_validation_errors'] = [];
      $elements['op']['#submit'] = [[self::class, 'removeSubmit']];
    }
    $elements['weight'] = [
      '#type' => 'weight',
      '#title' => $this->t('Weight'),
      '#title_display' => 'invisible',
      '#default_value' => $options['weight'] ?? 100,
      '#attributes' => ['class' => ['table-sort-weight']],
    ];
    return $elements;
  }

  /**
   * Validate attribute names.
   */
  public static function validateAttribute(array $element, FormStateInterface $form_state): void {
    if (!$element['name']['#value'] && $element['value']['#value']) {
      $form_state->setError($element['name'], t('Attribute name is required.'));
    }
    elseif ($element['name']['#value']) {
      if (in_array($element['name']['#value'], self::PROTECTED_ATTRIBUTES, TRUE)) {
        $form_state->setError($element['name'], t('Attribute name is protected and cannot be used.', [
          '%name' => $element['name']['#value'],
        ]));
      }
      elseif (preg_match('/^[\w-]+$/', $element['name']['#value']) === 0) {
        $form_state->setError($element['name'], t('Attribute contains invalid characters.'));
      }
    }
  }

  /**
   * Submit handler to add attribute.
   */
  public static function addSubmit(array $form, FormStateInterface $form_state): void {
    $values = $form_state->getvalue('attributes');
    if ($name = trim($values['add']['name'])) {
      unset($values['add']['name'], $values['add']['op']);
      $values[$name] = $values['add'];
    }
    $input = $form_state->getUserInput();
    unset($values['add'], $input['attributes']['add']);
    $form_state->setValue('attributes', $values);
    $form_state->setUserInput($input);
    $form_state->setRebuild();
  }

  /**
   * Submit handler to remove attribute.
   */
  public static function removeSubmit(array $form, FormStateInterface $form_state): void {
    $parents = $form_state->getTriggeringElement()['#array_parents'];
    array_pop($parents);
    $name = $parents[array_key_last($parents)];
    $values = $form_state->getvalue('attributes');
    $input = $form_state->getUserInput();
    unset($values[$name], $input['attributes'][$name]);
    $form_state->setValue('attributes', $values);
    $form_state->setUserInput($input);
    $form_state->setRebuild();
  }

  /**
   * Ajax callback for attributes.
   */
  public static function ajax(array $form, FormStateInterface $form_state): array {
    return $form['options']['attributes'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    if ($form_state->get('section') === 'attributes') {
      $values = $form_state->getValue('attributes');
      unset($values['add']);
      array_walk($values, function (&$options): void {
        unset($options['name'], $options['op']);
      });
      $this->options['attributes'] = $values;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    $options['attributes'] = [
      'category' => 'other',
      'title' => $this->t('Attributes'),
      'value' => $this->t('None'),
    ];
    if (isset($this->options['attributes']) && $count = count($this->options['attributes'])) {
      $options['attributes']['value'] = $this->t('Edit (@count)', [
        '@count' => $count,
      ]);
    }
  }

}
